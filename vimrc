" load indentation rules and plugins 
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" Enable syntax highlighting
if has("syntax")
  syntax on
endif

" Fix encoding
set encoding=utf-8  

" Define tab as 4 spaces
set tabstop=4

" Set one indent to one tab
set shiftwidth=4

" Insert space characters whenever the tab key is pressed
set expandtab

" Map Escape key to an easier location
imap jk <Esc> 
